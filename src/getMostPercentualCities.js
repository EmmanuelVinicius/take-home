const axios = require("axios");

async function getMostPercentualCities(startDateObj, endDateObj) {
    const list = [] 
    if (startDateObj.length == 0 || endDateObj.length == 0)
        return list;

    var mapped = endDateObj.map((item, i) => {
        let cities = {};

        if (item.city && item.estimated_population_2019) {
            const comparer = startDateObj.find(city => city.city == item.city);

            cities = {
                index: i,
                name: item.city,
                cases: (item.confirmed - (comparer ? comparer.confirmed : 0)),
                population: item.estimated_population_2019,
                percent: 0
            };
        }

        return cities;
    })

    mapped.sort(function (a, b) {
        a.percent = (a.cases / a.population) * 100;
        b.percent = (b.cases / b.population) * 100;
        if (a.percent < b.percent) {
            return 1;
        }
        if (a.percent > b.percent) {
            return -1;
        }
        return 0;
    });

    for (let i = 0; i < 10; i++) {
        list.push(new city(i, mapped[i].name, `${parseFloat(mapped[i].percent.toFixed(4))}%`));


        await axios({
            method: 'POST',
            url: `https://us-central1-lms-nuvem-mestra.cloudfunctions.net/testApi`,
            data: {
                id: i,
                nomeCidade: mapped[i].name,
                percentualDeCasos: `${parseFloat(mapped[i].percent.toFixed(4))}%`
            },
            headers: {
                'MeuNome': 'Emmanuel V]inicius'
            }

        })
    }

    return list;
}

const city = function (id, nomeCidade, percentualDeCasos) {
    return {
        id: id,
        nomeCidade: nomeCidade,
        percentualDeCasos: percentualDeCasos
    };
};

module.exports = getMostPercentualCities;