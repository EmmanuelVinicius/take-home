const Hapi = require('@hapi/hapi');
const axios = require("axios");
const Boom = require('boom');
const Joi = require('@hapi/joi')
    .extend(require('@hapi/joi-date'));

const getMostPercentualCities = require('./getMostPercentualCities');
const dateFormatter = require('./dateFormatter')

const failAction = (request, headers, erro) => {
    throw erro;
}

module.exports = [
    {
        method: 'GET',
        path: '/cities',
        config: {
            description: 'Captura os parâmetros para pesquisa',
            tags: ['api'],
            notes: 'É necessário passar os parâmetros de estado, data inicial e final.',
            validate: {
                failAction,
                query: Joi.object({
                    state: Joi.string().min(2).required(),
                    dateStart: Joi.date().format('YYYY-MM-DD').greater('2020-02-24').less(Joi.ref('dateEnd')).required(),
                    dateEnd: Joi.date().format('YYYY-MM-DD').less(Date.now()).required()
                })
            }
        },
        handler: async request => {
            try {
                const { state, dateStart, dateEnd } = request.query;
                const getURL = `https://brasil.io/api/dataset/covid19/caso/data/?state=${state}&date=`;

                const startDate = await axios.get(getURL + dateFormatter(dateStart));
                const endDate = await axios.get(getURL + dateFormatter(dateEnd));

                const result = getMostPercentualCities(startDate.data.results, endDate.data.results);
                return result;

            } catch (error) {
                return Boom.internal(error.message)
            }
        }
    }
]
