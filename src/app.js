const Hapi = require('@hapi/hapi');
const HapiSwagger = require('hapi-swagger');
const Vision = require('vision');
const Inert = require('inert');

const routes = require('./routes');
const app = new Hapi.Server({ port: 5000 })


async function main() {

    const swaggerOptions = {
        info: {
            title: 'Maior percentual de casos de COVID-19 por estado',
            version: 'v1.0'
        },
        lang: 'pt'
    }
    await app.register([
        Vision,
        Inert,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    app.route(routes);

    await app.start();
    return app;
}
module.exports = main();
