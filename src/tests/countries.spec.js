const assert = require('assert');
const app = require('./../app');

const headers = {
    MeuNome: 'Emmanuel Vinicius'
};

describe('Most covid cases by population', function () {
    this.timeout(Infinity);
    this.beforeAll(async () => {
        api = await app;
    });
    it('Should bring 10 results', async () => {
        const result = await api.inject({
            method: 'GET',
            headers,
            url: `/cities?state=MG&dateStart=2020-05-10&dateEnd=2020-05-18`
        });

        const data = JSON.parse(result.payload);
        const statusCode = result.statusCode;

        assert.deepEqual(statusCode, 200);
        assert.ok(Array.isArray(data));
    });
    it('Should not bring any result', async () => {
        const result = await api.inject({
            method: 'GET',
            headers,
            url: '/cities'
        });

        const statusCode = result.statusCode;

        assert.notDeepEqual(statusCode, 200);
    });
    it('Should bring empty by wrong state', async () => {
        const result = await api.inject({
            method: 'GET',
            headers,
            url: `/cities?state=GM&dateStart=2020-05-10&dateEnd=2020-05-18`
        });

        const data = JSON.parse(result.payload);
        const statusCode = result.statusCode;

        assert.deepEqual(statusCode, 200);
        assert.ok(data.length == 0);
    });
    it('Should fail for invalid date', async () => {
        const result = await api.inject({
            method: 'GET',
            headers,
            url: `/cities?state=MG&dateStart=10/05/2020=&dateEnd=2020-05-18`
        });

        const statusCode = result.statusCode;

        assert.notDeepEqual(statusCode, 200);
    });
    it('Should fail for greater date', async () => {
        const result = await api.inject({
            method: 'GET',
            headers,
            url: `/cities?state=MG&dateStart=2020-05-10&dateEnd=2020-10-18`
        });

        const statusCode = result.statusCode;

        assert.notDeepEqual(statusCode, 200);
    });
    it('Should fail for less date', async () => {
        const result = await api.inject({
            method: 'GET',
            headers,
            url: `/cities?state=MG&dateStart=2019-05-10&dateEnd=2020-05-18`
        });

        const statusCode = result.statusCode;

        assert.notDeepEqual(statusCode, 200);
    });
})
